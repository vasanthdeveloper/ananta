export interface ConfigImpl {
    captureSelector: string;
    container: string;
    buffer: number;
    automatic: boolean;
    quiet: boolean;
    limit: number;
    initCount: number;
    uri?: string;
}
export interface LocalStorageImpl {
    loadCount: number;
}
//# sourceMappingURL=interfaces.d.ts.map