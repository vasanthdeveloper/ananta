// Contains code for event emitters
import EventEmitter from 'events'

// Create an instance of EventEmitter class
const event = new EventEmitter()

export default event
