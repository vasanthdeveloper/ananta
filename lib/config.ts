import { ConfigImpl } from './interfaces'

// defaultConfig is the variable that holds default configuration
const defaultConfig: ConfigImpl = {
    captureSelector: 'article',
    container: 'main',
    buffer: 300,
    automatic: true,
    quiet: false,
    limit: 3,
    initCount: 2,
}

export default defaultConfig
