// This file will handle the loading and insertion of data

import { ConfigImpl, LocalStorageImpl } from './interfaces'
import event from './events'

let loading = false
let ticking = false
const lastWindowHeight = window.innerHeight
let lastDocumentHeight = document.documentElement.scrollHeight
let tempCount = 1

function handleData(
    DOM: XMLHttpRequest,
    ticking: boolean,
    currentConfig: ConfigImpl,
    localStorage: LocalStorageImpl,
): void {
    // Detach the onscroll handler if the server responded with anything
    // other than 200
    if (DOM.status != 200) {
        window.onscroll = null
        loading = false
        event.emit('fail', DOM.status)
    } else {
        // Now that we are sure the server responded with the requested
        // document, let's append the captured components to the container
        const containerElement = document.querySelector(currentConfig.container)
        const components = DOM.response.querySelectorAll(currentConfig.captureSelector)

        components.forEach(function(component) {
            containerElement.appendChild(component)
        })

        // Sync the current status
        lastDocumentHeight = document.documentElement.scrollHeight
        ticking = false
        loading = false

        // Increment the counter
        localStorage.loadCount++
        tempCount++

        // Call the triggerCallback function
        event.emit('done', localStorage.loadCount - currentConfig.initCount)
    }
}

export function fetchData(currentConfig: ConfigImpl, lastScrollY: number, localStorage: LocalStorageImpl): void {
    // Don't do anything if localStorage.loadCount exceeds currentConfig.limit
    if (currentConfig.limit > 0 && tempCount > currentConfig.limit) {
        window.onscroll = null
        return
    } else {
        // Do nothing if already loading
        if (loading) {
            return
        }

        // MAGIC: Check if the user scrolled till buffer
        if (
            currentConfig.automatic == true &&
            lastScrollY + lastWindowHeight <= lastDocumentHeight - currentConfig.buffer
        ) {
            ticking = false
            return
        }

        // Start send the GET request
        loading = true
        const requestHandler = new window.XMLHttpRequest()
        requestHandler.responseType = 'document'

        // Link the required events
        requestHandler.onload = function(): void {
            handleData(this, ticking, currentConfig, localStorage)
        }

        // Send the HTTP GET request
        const requestURI = currentConfig.uri.replace('<i>', localStorage.loadCount.toString())
        event.emit('sent', requestURI)
        requestHandler.open('GET', requestURI)
        requestHandler.send()
    }
}

// tick() will fire on scroll but won't fire when ticking is true
export function tick(currentConfig: ConfigImpl, lastScrollY: number, localStorage: LocalStorageImpl): void {
    ticking ||
        window.requestAnimationFrame(function() {
            fetchData(currentConfig, lastScrollY, localStorage)
        })
}

// resetCounter() will reset the tempCounter
export function resetCounter(): void {
    tempCount = 1
}
